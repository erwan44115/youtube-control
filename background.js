let currentTab = null;

let timeChoosen = 10;
let title = "Gardez le controle !";
let message = "Vous avez passé plus de temps que prévu sur youtube, vous êtes vraiment sûr de vouloir continuer ?";

browser.tabs.onActivated.addListener((event) => currentTab = event.tabId);
 
setInterval(updateBrowseTime, 1000);
setInterval(alertAddiction, 90000)

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;


async function updateBrowseTime() {
  if (!currentTab)
    return;
  let frames = null;
  try {
    frames = await browser.webNavigation.getAllFrames({ 'tabId': currentTab});
  } catch (error) {
    console.log(error);
  }
 
  let frame = frames.filter((frame) => frame.parentFrameId == -1)[0];
 
  if (!frame.url.startsWith('http'))
    return;
 
  let hostname = new URL(frame.url).hostname;
 
  try {
    let seconds = await browser.storage.local.get({[hostname]: 0});
    let dateObject = await browser.storage.local.get("date");
    if(hostname == "www.youtube.com"){
        if(dateObject.date == today){
            browser.storage.local.set({[hostname]: seconds[hostname] + 1});
        }
        else{
            browser.storage.local.set({[hostname]: 0});
            browser.storage.local.set({"date": today});
        }
    }
  } catch (error) {
    console.log(error);
  }
}

async function alertAddiction(){
  if (!currentTab)
    return;
 
  let frames = null;
  try {
    frames = await browser.webNavigation.getAllFrames({ 'tabId': currentTab});
  } catch (error) {
    console.log(error);
  }
 
  let frame = frames.filter((frame) => frame.parentFrameId == -1)[0];
 
  if (!frame.url.startsWith('http'))
    return;
 
  let hostname = new URL(frame.url).hostname;
  let seconds = await browser.storage.local.get({[hostname]: 0});  
  let timeC = await browser.storage.local.get("timeChosen");
  var minutesYt = Math.floor(seconds[hostname] / 60);
  if(hostname == "www.youtube.com"){
        console.log(timeC.timeChosen);
        console.log(minutesYt);
        if(minutesYt >= timeC.timeChosen){
              browser.notifications.create({
              "type": "basic",
              "iconUrl": browser.extension.getURL("icons/alerte.png"),
              "title": title,
              "message": message
             });
        }
  }

}

