let timeChoosen = 30;
timeElement = document.getElementById("timeChosen");

browser.storage.local.get().then(function (result) {
  let timeYt = 0;
 
  for (const key of Object.keys(result)) {
    if(key == "www.youtube.com"){
        timeYt = result[key];
    }
    if(key == "timeChosen"){
        timeChoosen = result[key];
        console.log(timeChoosen);
    }
  }
  var minutesYt = Math.floor(timeYt / 60);
  spanTemps = document.getElementById("temps-yt");
  spanTemps.innerText = minutesYt;
  spanTemps.className = "yt-green";

  if(minutesYt > 30){
      spanTemps.className = "yt-red";
  }
  document.getElementById("displayTime").innerText = timeChoosen;
  timeElement.value = timeChoosen;

});



async function changeTime(time){
    timeChoosen = time;
    document.getElementById("displayTime").innerText = timeChoosen;
    browser.storage.local.set({"timeChosen": timeChoosen});
}

timeElement.addEventListener("change", function(){
  changeTime(timeElement.value);
});
