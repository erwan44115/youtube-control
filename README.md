## Youtube control

Extension firefox pour tenter de controler le temps passé sur youtube. 

![alerte](https://cdn.discordapp.com/attachments/540888354968043520/805253366883155968/Capture.PNG)

![popup](https://cdn.discordapp.com/attachments/540888354968043520/805253542461177906/Capture2.PNG)

Lien firefox : https://addons.mozilla.org/fr/firefox/addon/youtubecontrol/
